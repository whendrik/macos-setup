# macOS setup

Settings & App's for your new macOS

* [SublimeText](https://www.sublimetext.com/) with [gruvbox](https://packagecontrol.io/packages/gruvbox)
* [OhMyZsh](https://github.com/ohmyzsh/ohmyzsh)
* [Terminal Themes](https://github.com/lysyi3m/macos-terminal-themes) with `broadcast`, fontsize 12; 120 cols, 26 rows
* [Task Explorer](https://objective-see.com/)
* [Mini Conda](https://conda.io)
* [pdbpp](https://pypi.org/project/pdbpp/) for `pytest`

## Setup Sublime

in `.zshrc`
```
export PATH="/Applications/Sublime Text.app/Contents/SharedSupport/bin":$PATH
```

## Git bug in OhMyZsh

https://superuser.com/questions/458906/zsh-tab-completion-of-git-commands-is-very-slow-how-can-i-turn-it-off
